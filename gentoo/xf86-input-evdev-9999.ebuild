# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit linux-info xorg-3 git-r3

HOMEPAGE="https://gitlab.com/at-home-modifier/at-home-modifier-evdev/wikis/home"
DESCRIPTION="Generic Linux input driver with at-home-modifier hack"
KEYWORDS="alpha ~amd64 arm hppa ia64 ~mips ppc ppc64 sh sparc x86"
IUSE=""
XORG_EAUTORECONF="yes"

EGIT_REPO_URI="https://gitlab.com/at-home-modifier/at-home-modifier-evdev.git"
# EGIT_BRANCH="master"
# Set this if you compile from a branch other than master.
# EGIT_BRANCH="my-branch"
# Set this if you compile from a commit other than HEAD.
# EGIT_COMMIT="1234abcd"

RDEPEND=">=x11-base/xorg-server-1.18[udev]
	dev-libs/libevdev
	sys-libs/mtdev
	virtual/libudev:="
DEPEND="${RDEPEND}
	>=sys-kernel/linux-headers-2.6
	x11-base/xorg-proto"

pkg_pretend() {
	if use kernel_linux ; then
		CONFIG_CHECK="~INPUT_EVDEV"
	fi
	check_extra_config
}

src_unpack(){
	git-r3_src_unpack
}

src_install() {
	xorg-3_src_install
	dodoc README*
}
