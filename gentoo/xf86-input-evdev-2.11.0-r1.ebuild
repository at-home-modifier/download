# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

XORG_EAUTORECONF=yes
XORG_TARBALL_SUFFIX="xz"

inherit linux-info xorg-3

HOMEPAGE="https://gitlab.com/at-home-modifier/at-home-modifier-evdev/wikis/home"
DESCRIPTION="Generic Linux input driver with at-home-modifier hack"
KEYWORDS="alpha amd64 arm ~arm64 hppa ia64 ~mips ppc ppc64 ~sh sparc x86"
IUSE=""

SRC_URI="${SRC_URI} https://gitlab.com/at-home-modifier/download/-/raw/master/patch/ahm-${PV}.patch"

RDEPEND="
	>=x11-base/xorg-server-1.18[udev]
	dev-libs/libevdev
	sys-libs/mtdev
	virtual/libudev:="
DEPEND="
	${RDEPEND}
	>=sys-kernel/linux-headers-2.6
	x11-base/xorg-proto"

pkg_pretend() {
	if use kernel_linux ; then
		CONFIG_CHECK="~INPUT_EVDEV"
	fi
	check_extra_config
}

src_prepare() {
	local PATCHES=( "${DISTDIR}/ahm-${PV}.patch" )
	xorg-3_src_prepare
}

src_install() {
	xorg-3_src_install
	dodoc README*
}
